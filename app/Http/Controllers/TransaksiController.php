<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;

class TransaksiController extends Controller
{
    public function index()
    {
    	// mengambil data dari table transaksi

    	$transaksi = DB::table('T_TRANSAKSI')
    				->select('T_TRANSAKSI.*','NAMA_USAHA')
                    ->join(DB::raw('(SELECT * FROM USERS) U'),
                    function($join){
                        $join->on('T_TRANSAKSI.ID_TEMPAT_USAHA', '=', 'U.ID_TEMPAT_USAHA');
                    })
    				->orderBy('TANGGAL_TRX','DESC')
    				->get();
 
    	// mengirim data pegawai ke view index
    	return view('transaksi.index',['transaksi' => $transaksi]);
 
    }

    public function rekapbulanan()
    {
    	// mengambil data dari table transaksi

    	$transaksi = DB::table('T_TRANSAKSI')
    				->select('T_TRANSAKSI.*','NAMA_USAHA')
                    ->join(DB::raw('(SELECT * FROM USERS) U'),
                    function($join){
                        $join->on('T_TRANSAKSI.ID_TEMPAT_USAHA', '=', 'U.ID_TEMPAT_USAHA');
                    })
    				->orderBy('TANGGAL_TRX','DESC')
    				->get();
 
    	// mengirim data pegawai ke view index
    	return view('transaksi.rekapbulanan',['transaksi' => $transaksi]);
 
    }

    public function transaksiwp()
    {
    	// mengambil data dari table transaksi

    	$transaksi = DB::table('T_TRANSAKSI')
    				->select('T_TRANSAKSI.*','NAMA_USAHA')
                    ->join(DB::raw('(SELECT * FROM USERS) U'),
                    function($join){
                        $join->on('T_TRANSAKSI.ID_TEMPAT_USAHA', '=', 'U.ID_TEMPAT_USAHA');
                    })
    				->orderBy('TANGGAL_TRX','DESC')
    				->get();
 
    	// mengirim data pegawai ke view index
    	return view('transaksi.transaksiwp',['transaksi' => $transaksi]);
 
    }

    public function wptidaktermonitor()
    {
    	// mengambil data dari table transaksi

    	$transaksi = DB::table('T_TRANSAKSI')
    				->select('T_TRANSAKSI.*','NAMA_USAHA')
                    ->join(DB::raw('(SELECT * FROM USERS) U'),
                    function($join){
                        $join->on('T_TRANSAKSI.ID_TEMPAT_USAHA', '=', 'U.ID_TEMPAT_USAHA');
                    })
    				->orderBy('TANGGAL_TRX','DESC')
    				->get();
 
    	// mengirim data pegawai ke view index
    	return view('transaksi.wptidaktermonitor',['transaksi' => $transaksi]);
 
    }
}
