<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
            // $product = Product::count();
            // $order = Order::count();
            // $customer = Customer::count();
            // $user = User::count();
            // return view('home', compact('product', 'order', 'customer', 'user'));
            return view('home');
        // session()->flash('flash_message', 'Sukses Login');
        // session()->flash('flash_type', 'success');
        // return view('home');
        // $users=DB::select('select* from data_transaksi');
        // var_dump($users);
    }
}
