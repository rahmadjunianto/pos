<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;
use DB;

class UserController extends Controller
{
    public function index()
    {
        //dd(auth()->user());
        $users = User::orderBy('created_at', 'DESC')->paginate(10);
        return view('users.index', compact('users'));

    }

    public function create()
    {
        $role = Role::orderBy('name', 'ASC')->get();
        return view('users.create', compact('role'));
    }

    public function store(Request $request)
    {
        $kode_aktivasi = mt_rand(1000,9999);

        $this->validate($request, [
            'nama_wp'       => 'required|string',
            'npwpd'         => 'required|string',
            'nama_usaha'    => 'required|string',
            // 'name'     => 'required|string|max:100',
            'email'         => 'required|email|unique:users',
            'password'      => 'required|min:4',
            'role'          => 'required|string|exists:roles,name'
        ]);

        $user = User::firstOrCreate([
            'email' => $request->email
        ], [
            'name'            => $request->nama_wp,
            'username'        => $request->npwpd,
            'kode_aktivasi'   => $kode_aktivasi,
            'id_tempat_usaha' => $request->id_usaha,
            'nama_usaha'      => $request->nama_usaha,
            'label_app'       => 'POS-Restoran',
            'theme_app'       => '0',
            'password'        => bcrypt($request->password),
            'status'          => true
        ]);

        $user->assignRole($request->role);
        return redirect(route('users.index'))->with(['success' => 'User: <strong>' . $user->name . '</strong> Ditambahkan']);
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('users.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            // 'name' => 'required|string|max:100',
            'nama_wp'    => 'required|string',
            'npwpd'      => 'required|string',
            'nama_usaha' => 'required|string',
            'email'      => 'required|email|exists:users,email',
            'password'   => 'nullable|min:4',
        ]);

        $user = User::findOrFail($id);
        $password = !empty($request->password) ? bcrypt($request->password):$user->password;
        $user->update([
            'name'            => $request->nama_wp,
            'username'        => $request->npwpd,
            'id_tempat_usaha' => $request->id_usaha,
            'nama_usaha'      => $request->nama_usaha,
            'password'        => $password
        ]);
        return redirect(route('users.index'))->with(['success' => 'User: <strong>' . $user->name . '</strong> Diperbaharui']);
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect()->back()->with(['success' => 'User: <strong>' . $user->name . '</strong> Dihapus']);
    }

    public function roles(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $roles = Role::all()->pluck('name');
        return view('users.roles', compact('user', 'roles'));
    }

    public function setRole(Request $request, $id)
    {
        $this->validate($request, [
            'role' => 'required'
        ]);

        $user = User::findOrFail($id);
        $user->syncRoles($request->role);
        return redirect()->back()->with(['success' => 'Role Sudah Di Set']);
    }

    public function rolePermission(Request $request)
    {
        $role = $request->get('role');
        $permissions = null;
        $hasPermission = null;

        $roles = Role::all()->pluck('name');

        if (!empty($role)) {
            $getRole = Role::findByName($role);
            $hasPermission = DB::table('role_has_permissions')
                ->select('permissions.name')
                ->join('permissions', 'role_has_permissions.permission_id', '=', 'permissions.id')
                ->where('role_id', $getRole->id)->get()->pluck('name')->all();
            $permissions = Permission::all()->pluck('name');
        }
        return view('users.role_permission', compact('roles', 'permissions', 'hasPermission'));
    }

    public function addPermission(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:permissions'
        ]);

        $permission = Permission::firstOrCreate([
            'name' => $request->name
        ]);
        return redirect()->back();
    }

    public function setRolePermission(Request $request, $role)
    {
        $role = Role::findByName($role);
        $role->syncPermissions($request->permission);
        return redirect()->back()->with(['success' => 'Permission to Role Saved!']);
    }

    function fetch(Request $request)
    {
        if($request->get('query'))
        {
            $query = $request->get('query');
            $data = DB::table('wajib_pajak')
                    ->select('nama','npwpd','nama_usaha','TU.id_inc')
                    ->join(DB::raw('(SELECT * FROM TEMPAT_USAHA WHERE JENIS_PAJAK=02) TU'),
                    function($join){
                        $join->on('WAJIB_PAJAK.NPWP', '=', 'TU.NPWPD');
                    })
                    ->where('nama', 'LIKE', "%{$query}%")
                    ->orderBy('wajib_pajak.nama','ASC')
                    ->get();
            $output = '<ul class="dropdown-menu" style="display:block; position:relative; list-style: none;  padding: 0;">';
            foreach($data as $row)
            {
                $output .= '
                <li style="padding: 5px 10px;  background-color: #EEEEEE; border: 1px solid #DDDDDD; font: Helvetica, Verdana, sans-serif;" data-val="'.$row->npwpd.'#'.$row->nama.'#'.$row->nama_usaha.'#'.$row->id_inc.'""><a href="#">'.$row->nama.' <i>('.$row->nama_usaha.')</i></a></li>
                ';
            }
            $output .= '</ul>';
            echo $output;
        }
    }

    function numberRandom(Request $request){
        echo mt_rand(1000,9999);
    }
}
