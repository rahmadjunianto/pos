@extends('layouts.master')

@section('title')
    <title>Transaksi</title>
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Transaksi</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item active">Transaksi</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                            @card
                            @slot('title')
                            <div class="row">
                              <div class="col-3">
                                <input type="text" class="form-control" placeholder=".col-3">
                              </div>
                              <div class="col-4">
                                <input type="text" class="form-control" placeholder=".col-4">
                              </div>
                              <div class="col-5">
                                <input type="text" class="form-control" placeholder=".col-5">
                              </div>
                            </div>
                            @endslot

                            @if (session('error'))
                                @alert(['type' => 'danger'])
                                    {!! session('error') !!}
                                @endalert
                            @endif
                            <div class="table-responsive">
                            <table id="example1" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <td align="center">#</td>
                                    <td align="center">Tanggal Trx</td>
                                    <td align="center">Nama Usaha</td>
                                    <td align="center">Disc (%)</td>
                                    <td align="center">Omzet</td>
                                </tr>
                                </thead>
                                <tbody>
                                @php $no = 1; $total = 0;@endphp
                                @forelse ($transaksi as $row)
                                <tr>
                                    <td align="center">{{ $no++ }}</td>
                                    <td align="center">{{ date('d-m-Y H:i:s', strtotime($row->tanggal_trx)) }}</td>
                                    <td>{{ $row->nama_usaha }}</td>
                                    <td align="center">{{ $row->disc }}</td>
                                    <td align="right">{{ 'Rp. '.number_format($row->omzet,0, ',' , '.')}}</td>  
                                </tr>
                                @php $total+=$row->omzet; @endphp
                                @empty
                                <tr>
                                    <td colspan="5" class="text-center">Tidak ada data</td>
                                </tr>                                        
                                @endforelse
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="4" class="text-center"><b>Total</b></td>
                                    <td class="text-right"><b>{{ 'Rp. '.number_format($total,0, ',' , '.')}}</b></td>
                                </tr>
                                </tfoot>
                                </table>
                            </div>
                            @slot('footer')

                            @endslot
                        @endcard
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection