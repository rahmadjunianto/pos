@extends('layouts.master')

@section('title')
    <title>POS-Tambah Pengguna</title>
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Tambah Pengguna</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('users.index') }}">Pengguna</a></li>
                            <li class="breadcrumb-item active">Tambah</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        @card
                            @slot('title')
                            
                            @endslot
                            
                            @if (session('error'))
                                @alert(['type' => 'danger'])
                                    {!! session('error') !!}
                                @endalert
                            @endif
                            
                            <form action="{{ route('users.store') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label for="">Nama WP</label>
                                    <input type="text" name="nama_wp" id="nama_wp" class="form-control {{ $errors->has('nama_wp') ? 'is-invalid':'' }}" required>
                                    <div id="wpList">
                                    </div>
                                    <p class="text-danger">{{ $errors->first('nama_wp') }}</p>
                                </div>
                                <div class="form-group">
                                    <label for="">NPWPD</label>
                                    <input type="text" name="npwpd" id="npwpd" class="form-control {{ $errors->has('npwpd') ? 'is-invalid':'' }}" required readonly>
                                    <p class="text-danger">{{ $errors->first('npwpd') }}</p>
                                </div>
                                <div class="form-group">
                                    <label for="">Nama Usaha</label>
                                    <input type="text" name="nama_usaha" id="nama_usaha" class="form-control {{ $errors->has('nama_usaha') ? 'is-invalid':'' }}" required readonly>
                                    <input type="hidden" name="id_usaha" id="id_usaha" class="form-control">
                                    <p class="text-danger">{{ $errors->first('nama_usaha') }}</p>
                                </div>
                                <!-- <div class="form-group">
                                    <label for="">Nama</label>
                                    <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" required>
                                    <p class="text-danger">{{ $errors->first('name') }}</p>
                                </div> -->
                                <div class="form-group">
                                    <label for="">Email</label>
                                    <input type="email" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid':'' }}" required>
                                    <p class="text-danger">{{ $errors->first('email') }}</p>
                                </div>
                                <div class="form-group">
                                    <label for="">Password</label>
                                    <input type="password" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid':'' }}" required>
                                    <p class="text-danger">{{ $errors->first('password') }}</p>
                                </div>
                                <div class="form-group">
                                    <label for="">Role</label>
                                    <select name="role" class="form-control {{ $errors->has('role') ? 'is-invalid':'' }}" required>
                                        <option value="">Pilih</option>
                                        @foreach ($role as $row)
                                        <option value="{{ $row->name }}">{{ $row->name }}</option>
                                        @endforeach
                                    </select>
                                    <p class="text-danger">{{ $errors->first('role') }}</p>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary btn-sm">
                                        <i class="fa fa-send"></i> Simpan
                                    </button>
                                </div>
                            </form>
                            @slot('footer')

                            @endslot
                        @endcard
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection